# extraction


## Présentation du format GFA 

`https://gfa-spec.github.io/GFA-spec/GFA1.html`

TSV avec plusieurs type de ligne dont la première colonne spécifie le type d'éléments
(S) SEGMENT = noeud du graphe
(L) LINK = arc 
(W) WALK = chemin dans 
(P) PATH = walk sans arête répétée

## Objectif1 
A partir d'un GFA extraire tous les noeuds spécifiques d'un groupe d'haplotypes pour tout le graphe ou pour une région localisée entre 2 coordonnées sur une des références.

### Outil d'Alexis : à l'aide d'un dico, on peut extraire tous les noeuds spécifique à un haplotype. (GFAparser.py + get_unique_nodes.py) [UniP](https://forgemia.inra.fr/alexis.mergez/unip)

Il parcourt tout le GFA et génère un dictionnaire qui renvoie pour chaque noeud une liste de PATH dans lequel on le retrouve

Possibilité d'utiliser l'API https://github.com/Tharos-ux/gfagraphs, doc pas encore disponible

VG oblige de changer de format, et pose de problème d'indexation (pour le mapping).

Utilisation de vg deconstruct (possibilité de tester les 2)

* Possibilité de générer une matrice de présence/absence avec les noeuds en ligne et les haplotypes en colonnes, voire remplacer les présence/absence par une abondance (nombre de fois ou on observe ce noeud dans le path)  

### On fait 2 ateliers :
* génération/utilisation de la matrice générée par UniP
* utilisation de vg deconstruct pour produire un VCF et manipulation du VCF.
* création de csv qui permettent de décrire la composition des bubble (noeud PGGB) en SV (position chez différent individus). 

Jeu de données : zymo ou un gfa au choix (utilisation de MGC et Pan1c), les données sont disponibles ici : https://forgemia.inra.fr/panannot/add_annotation

### MODE UniP
```
git clone git@forgemia.inra.fr:alexis.mergez/unip.git
cd unip
python3 UniP.py --gfa ztritici_19g_pggb.gfa.gz --output UniP_Ztritici.tsv
```
### RECUPERATION DE PANGETOOLS :
```
singularity pull pangetools.sif  oras://registry.forgemia.inra.fr/alexis.mergez/pangetools/pangetools:latest
```

### MODE VG 
 génération de la vg à partir du docker :
 ```
 singularity pull docker://quay.io/vgteam/vg:v1.58.0
```

* mode pggb :

    * convert le graphe gfa en xg :
```
singularity  exec vg_v1.58.0.sif vg convert -g ztritici_19g_pggb.gfa  -x > ztritici_19g_pggb.xg
singularity  exec vg_v1.58.0.sif vg deconstruct  -p IPO323#1#chr1#0 ztritici_19g_pggb.xg > ztritici_19g_pggb.vcf
```

    * avec odgi extract : 
```
singularity exec  pangetools.sif  odgi extract -i ztritici_19g_pggb.gfa -o ztritici_19g_pggb.sub100k200k.og -P -r IPO323#1#chr1:100000-200000 -c20 -d 0 -E
singularity exec  pangetools.sif  odgi view -i ztritici_19g_pggb.sub100k200k.og -g -t2  -P > ztritici_19g_pggb.sub100k200k.gfa 
```
(Attention n'oubliez pas de binder les répertoires nécessaire si besoin : singularity exec -B /works/...)

**Attention ne pas utiliser vg view.**

* mode minigraph-cactus :

```
# imgae vg last version :
singularity pull vg_v1.58.0.sif docker://quay.io/vgteam/vg:v1.58.0

# convert GFA en XG :
srun -c 8 --mem=32G --tmp=50G singularity exec vg_v1.58.0.sif vg convert -t 8 -x ztritici_19g_mc.gfa > ztritici_19g_mc.xg

# convert GFA en VG :
srun -c 8 --mem=32G --tmp=50G singularity exec vg_v1.58.0.sif vg convert -t 8 -p ztritici_19g_mc.gfa > ztritici_19g_mc.vg

# pour obtenir le nom de la reference:
srun -c 8 --tmp=50G --mem=32G singularity exec vg_v1.58.0.sif vg paths -x ztritici_19g_mc.xg -L > ztritici_19g_mc.paths

# appel de variants de pangenome - produit un VCF :
srun -c 8 --mem=32G --tmp=50G singularity exec vg_v1.58.0.sif vg deconstruct -t 8 -p IPO323#IPO323.chr_1 ztritici_19g_mc.xg > ztritici_19g_mc.vcf
```


### Extraction d'une région (IP0323 100k - 200k)

* mode pggb :

```
singularity exec vg_v1.58.0.sif vg chunk  -c 10  -p 'IPO323#1#chr1#0:100000-200000' -x  ztritici_19g_pggb.xg  > ztritici_19g_pggb.sub.xg 
# Transformation en vcf
singularity  exec vg_v1.58.0.sif vg deconstruct  -p IPO323#1#chr1#0 ztritici_19g_pggb.sub.xg > ztritici_19g_pggb.sub.vcf 
```

**NB: Bien utiliser la version 1.58.0 de vg deconstruct pour obtenir un vcf avec les variants alternatifs des différents génotypes** 

    * avec odgi extract:
```
singularity exec  -B /scratch/flegeai ../containers/pangetools.sif  odgi extract -i ztritici_19g_pggb.gfa -o ztritici_19g_pggb.sub100k200k.og -P -r IPO323#1#chr1:100000-200000 -c20 -d 0 -E
singularity exec  -B /scratch/flegeai ../containers/pangetools.sif  odgi view -i ztritici_19g_pggb.sub100k200k.og -g -t2  -P > ztritici_19g_pggb.sub100k200k.gfa ztritici_19g_pggb.sub100k200k.gfa
```

* mode minigraph-cactus :

``` 
# vg chunk chr_1:100000-200000
## avec context-jaccard option -c 10
srun -c 8 --mem=32G --tmp=50G singularity exec vg_v1.58.0.sif vg chunk -x ztritici_19g_mc.xg -c 10 -p IPO323#IPO323.chr_1:100000-200000 -O pg > ztritici_19g_mc.chr1_100kb.vg

## test path avec #0# ==> resultat, pas de difference entre les 2 vcf selon path avec ou sans #0#
srun -c 8 --mem=32G --tmp=50G singularity exec vg_v1.58.0.sif vg chunk -x ztritici_19g_mc.xg -c 10 -p IPO323#0#IPO323.chr_1:100000-200000 -O pg > ztritici_19g_mc.0.chr1_100kb.vg

## avec context-jaccard option -c 0
srun -c 8 --mem=32G --tmp=50G singularity exec vg_v1.58.0.sif vg chunk -x ztritici_19g_mc.xg -c 0 -p IPO323#0#IPO323.chr_1:100000-200000 -O pg > ztritici_19g_mc.0.chr1_100kb_context-jacc0.vg

# appel de variants avec vg deconstruct (vcf)  a partir du chunk produit 
srun -c 8 --mem=64G --tmp=50G singularity exec vg_v1.58.0.sif vg deconstruct -t 8 ztritici_19g_mc.0.chr1_100kb_context-jacc0.vg > ztritici_19g_mc.0.chr1_100kb_context-jacc0.vcf

srun -c 8 --mem=64G --tmp=50G singularity exec vg_v1.58.0.sif vg deconstruct -t 8 ztritici_19g_mc.chr1_100kb.vg > ztritici_19g_mc.chr1_100kb.vcf
```

### Extraction d'une région (1A5 et UR95 chr1)

* mode minigraph-cactus :

```
# transfo des path en walk pour MC qui produit un gfa avec des lignes P seulement pour la ref et des lignes W pour les autres genomes
# option -f (output=gfa) obligatoire avec -W
srun -c 8 --mem=32G --tmp=50G singularity exec vg_v1.58.0.sif vg convert -W -t 8 -f ztritici_19g_mc.gfa > ztritici_19g_mc_allPaths.gfa

srun -c 8 --mem=32G --tmp=50G singularity exec vg_v1.58.0.sif vg convert -t 8 -x ztritici_19g_mc_allPaths.gfa > ztritici_19g_mc_allPaths.xg

srun -c 8 --tmp=50G --mem=32G singularity exec vg_v1.58.0.sif vg paths -x ztritici_19g_mc_allPaths.xg -L > ztritici_19g_mc_allPaths.paths

# obtention d'un vcf pour le chromosome1 avec pour reference le sample 1A5 au lieu de IPO323 (ref du gfa MC d'origine)
fgrep '1A5' ztritici_19g_mc_allPaths.paths

srun -c 8 --mem=32G --tmp=50G singularity exec vg_v1.58.0.sif vg chunk -t 8 -x ztritici_19g_mc_allPaths.xg -c 10 -p 1A5#0#1A5.chr_1#0[81351-6088472] -O pg > ztritici_19g_mc.chr1_1A5.vg

## pas d'output xg pour 'vg chunk' donc vg convert
srun -c 8 --mem=32G --tmp=50G singularity exec vg_v1.58.0.sif vg convert -t 8 -x ztritici_19g_mc.chr1_1A5.vg > ztritici_19g_mc.chr1_1A5.xg

srun -c 8 --mem=32G --tmp=50G singularity exec vg_v1.58.0.sif vg deconstruct -t 8 -p 1A5#0#1A5.chr_1#0[81351-6088472] ztritici_19g_mc.chr1_1A5.xg > ztritici_19g_mc.chr1_1A5.vcf

# obtention d'un vcf pour le chromosome1 avec pour reference le sample UR95 au lieu de IPO323 (ref du gfa MC d'origine)
fgrep 'UR5' ztritici_19g_mc_allPaths.paths

srun -c 8 --mem=32G --tmp=50G singularity exec vg_v1.58.0.sif vg chunk -t 8 -x ztritici_19g_mc_allPaths.xg -c 10 -p UR95#0#UR95.chr_1#0[14793-6120084] -O pg > ztritici_19g_mc.chr1_UR95.vg

## pas d'output xg pour 'vg chunk' donc vg convert
srun -c 8 --mem=32G --tmp=50G singularity exec vg_v1.58.0.sif vg convert -t 8 -x ztritici_19g_mc.chr1_UR95.vg > ztritici_19g_mc.chr1_UR95.xg

srun -c 8 --mem=32G --tmp=50G singularity exec vg_v1.58.0.sif vg deconstruct -t 8 -p UR95#0#UR95.chr_1#0[14793-6120084] ztritici_19g_mc.chr1_UR95.xg > ztritici_19g_mc.chr1_UR95.vcf
```

Recherche d'un noeud commun entre les vcf pour verifier la bonne correspondance des donnees:

```
## Recherche du noeud >9598>9601 dans les vcf ayant respect. pour ref IPO323, 1A5 et UR95 
grep '^#' ztritici_19g_mc.chr1_100kb.vcf
##fileformat=VCFv4.2
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
##INFO=<ID=CONFLICT,Number=.,Type=String,Description="Sample names for which there are multiple paths in the graph with conflicting alleles">
##INFO=<ID=AC,Number=A,Type=Integer,Description="Total number of alternate alleles in called genotypes">
##INFO=<ID=AF,Number=A,Type=Float,Description="Estimated allele frequency in the range (0,1]">
##INFO=<ID=NS,Number=1,Type=Integer,Description="Number of samples with data">
##INFO=<ID=AN,Number=1,Type=Integer,Description="Total number of alleles in called genotypes">
##INFO=<ID=AT,Number=R,Type=String,Description="Allele Traversal as path in graph">
##contig=<ID=IPO323#0#IPO323.chr_1,length=200225>
#CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT  1A5     1E4     3D1     3D7     Arg00   Aus01   CH95    CNR93   CRI10   I93     IR01_26b    IR01_48b        ISY92   KE94    OregS90 TN09    UR95    YEQ92
grep '>9598>9601' ztritici_19g_mc.chr1_100kb.vcf
IPO323#0#IPO323.chr_1   111411  >9598>9601      C       T       60      .       AC=1;AF=0.0625;AN=16;AT=>9598>9599>9601,>9598>9600>9601;NS=16   GT      0       .       0       0       0       0       .       0       0       0       0       0       0       0       0       0       1       0

grep '#CHROM' ztritici_19g_mc.chr1_1A5.vcf
#CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT  1E4     3D1     3D7     Arg00   Aus01   CH95    CNR93   CRI10   I93     IPO323  IR01_26b    IR01_48b        ISY92   KE94    OregS90 TN09    UR95    YEQ92
grep '>9598>9601' ztritici_19g_mc.chr1_1A5.vcf
1A5#0#1A5.chr_1 81627   >9598>9601      C       T       60      .       AC=1;AF=0.0769231;AN=13;AT=>9598>9599>9601,>9598>9600>9601;NS=13        GT      .       0       0       .       0       .       0       .       0       0       .       0       0       0       0       0       1       0

grep '#CHROM' ztritici_19g_mc.chr1_UR95.vcf
#CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT  1A5     1E4     3D1     3D7     Arg00   Aus01   CH95    CNR93   CRI10   I93     IPO323      IR01_26b        IR01_48b        ISY92   KE94    OregS90 TN09    YEQ92
grep '>9598>9601' ztritici_19g_mc.chr1_UR95.vcf
UR95#0#UR95.chr_1       15037   >9598>9601      T       C       60      .       AC=13;AF=1;AN=13;AT=>9598>9600>9601,>9598>9599>9601;NS=13       GT      1       .       1       1       .       1       .       1       .       1       1       .       1       1       1       1       1       1
```



* mode minigraph :

Calling structural variations
https://github.com/lh3/minigraph/tree/master?tab=readme-ov-file#calling-structural-variations

Given an assembly, you can find the path/allele of this assembly in each bubble of a gfa built with miniraph :
```minigraph -cxasm --call -t16 graph.gfa sample-asm.fa > sample.bed```

`nohup minigraph/minigraph -cxasm --call ztritici_19g_minigraph.gfa 3D1.fa  > minigraph_3D1.bed`

```
IPO323.chr_1	111566	111594	>s1	>s3	>s2:28:+:3D1.chr_1:68200:68242
IPO323.chr_1	116607	116818	>s3	>s5	>s45017:292:+:3D1.chr_1:72985:73598
IPO323.chr_1	117410	119188	>s5	>s9	>s6>s45018>s8:2417:+:3D1.chr_1:74071:76532
IPO323.chr_1	119868	119868	>s9	>s10	*:0:+:3D1.chr_1:77208:77212
IPO323.chr_1	120677	121080	>s10	>s12	>s32952:397:+:3D1.chr_1:78020:78456
```

je teste avec le gfa de minigraph cactus pour voir si ça marche aussi :

`nohup minigraph/minigraph -cxasm --call ztritici_19g_mc.gfa  3D1.fa  > mc_3D1.bed`


```
IPO323#IPO323.chr_1	109377	109377	>455	>456	.
IPO323#IPO323.chr_1	109515	109516	>456	>459	.
IPO323#IPO323.chr_1	109529	109530	>459	>462	.
IPO323#IPO323.chr_1	109635	109636	>462	>465	.
IPO323#IPO323.chr_1	109668	109669	>465	>468	.
```
=> ça marche :)

je teste sur le fichier gfa de pggb :

`nohup minigraph/minigraph -cxasm --call ztritici_19g_pggb.gfa  3D1.fa  > pggb_3D1.bed`

=> ça ne marche pas :(

### Comparaison des résultats PGGB vs MC

sur quelques exemples on retrouve des SNP identiques. Par contre, on observe des différences en cas de variations imbriquées.